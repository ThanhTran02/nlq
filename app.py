from flask import Flask, request, render_template
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
import numpy as np
import re
import pickle

app = Flask(__name__)

maxLength=200

tokenizer_file_path = './model/tokenizer.pkl'
with open(tokenizer_file_path, 'rb') as f:
    tokenizer = pickle.load(f)

model = load_model('./model/my_model.h5') 
def predict_sentiment(input_text, tokenizer, maxLength, model):
    input_text = clean_text(input_text)
    padded_sequences  = process_input(input_text)
    predictions = model.predict(padded_sequences)
    return predictions

 
@app.route('/',methods=['GET',"POST"])


def main():
    if request.method =='POST':
        input_text = request.form.get("textarea")
        result,senti = pre(input_text)
        senti = round(senti,2)
        if result[0] ==0:
            return render_template('home.html',message ="Negative 😤😤",input_text=input_text,sentiment =senti )
        else:
            return render_template('home.html',message ="Positive 😊😊",input_text=input_text,sentiment =senti)
    return render_template('home.html')


def process_input(data):
    test = tokenizer.texts_to_sequences([data])
    input_sequence = pad_sequences(test, padding='post' ,maxlen=maxLength)
    return input_sequence


def clean_text(input_text):
    input_text = re.sub(r'[^a-zA-Z\s]', "", input_text)
    input_text = re.sub(r'[^\w\s]', '', input_text)
    input_text = re.sub(r'http\S+', '', input_text)
    
    return input_text.lower()

def pre(input):
    prediction= predict_sentiment(input, tokenizer, maxLength,model)
    y_pred=np.where(prediction>=.5,1,0)
    return y_pred,prediction[0][0]
if __name__ == '__main__':
    app.run(debug=True, use_debugger=False, use_reloader=False)